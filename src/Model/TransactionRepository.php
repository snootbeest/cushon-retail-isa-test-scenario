<?php

namespace Tom\CushonRetailIsa\Model;

use DateTimeImmutable;

class TransactionRepository
{
    /**
     * Create a transaction
     * @param int $amount
     * @return array
     */
    protected function createTransaction (int $amount): array
    {
        return [
            'id'       => '879f1d32-cb92-4d06-953d-0d7c8293b44c',
            'dateTime' => (new DateTimeImmutable)->format('Y-m-d H:i:s')
        ];
    }
}