<?php

namespace Tom\CushonRetailIsa\Model;

use Exception;

class IsaRepository
{
    const TYPE_FUND = 'fund';
    const AVAILABLE_TYPES = [
        self::TYPE_FUND,
    ];

    /**
     * Creates a new ISA of type for the given customer
     * @param string $customerId
     * @param string $type
     * @param string $name
     * @return string[]
     */
    public function createIsa(
        string $customerId,
        string $type,
        string $name = 'Cushon ISA'): array
    {
        return [
            'id'         => '879f1d32-cb92-4d06-953d-0d7c8293b44c',
            'customerId' => $customerId,
            'type'       => self::TYPE_FUND,
            'name'       => $name,
            'totalValue' => 0,
            'yearToDate' => 0,
        ];
    }

    /**
     * Get the isa type for the given customer
     * @param string $customerId
     * @param string $type
     * @return string[]
     * @throws Exception
     */
    public function getIsa(string $customerId, string $type): array
    {
        if (in_array($type, IsaRepository::AVAILABLE_TYPES) === false) {
            throw new Exception(sprintf('type %s not supported', $type));
        }

        return [
            'id'         => '73a999a8-962c-42d4-9aff-7a329bc5db7c',
            'customerId' => $customerId,
            'type'       => self::TYPE_FUND,
            'name'       => 'Cushon ISA',
            'value'      => 100,
            'yearToDate' => 0
        ];
    }
}