<?php

namespace Tom\CushonRetailIsa\Model;

class IsaFundTransactionRepository extends TransactionRepository
{
    /**
     * Create an isa/fund transaction
     * @param int $amount
     * @param string $isaId
     * @param string $fundId
     * @return array
     */
    public function createIsaFundTransaction(int $amount, string $isaId, string $fundId): array
    {
        $transaction = $this->createTransaction($amount);

        return array_merge(
            $transaction,
            [
                'transaction'          => $transaction,
                'isaFundTransactionId' => '9a785c52-7948-467f-abbb-4b7fe4e1ff53',
                'isaId'                => $isaId,
                'fundId'               => $fundId,
            ]
        );
    }
}