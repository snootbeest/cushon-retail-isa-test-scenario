USE retail_isa;
INSERT IGNORE INTO fund (id, name) VALUES (UUID(), "Cushon Equities Fund");

INSERT IGNORE INTO customer (
    id,
    nino,
    title,
    firstName,
    lastName
) VALUES (
    UUID(),
    "AB123456A",
    "Mr",
    "Existing",
    "Customer"
);