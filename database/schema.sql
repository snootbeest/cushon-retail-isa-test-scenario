DROP DATABASE IF EXISTS retail_isa;
CREATE DATABASE retail_isa;
USE retail_isa;

CREATE TABLE IF NOT EXISTS fund (
    id UUID NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS customer (
    id UUID NOT NULL PRIMARY KEY,
    nino VARCHAR(9) NOT NULL UNIQUE,
    title VARCHAR(255) DEFAULT '',
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS isa (
    id UUID NOT NULL PRIMARY KEY,
    customerId UUID REFERENCES customer(id),
    name varchar(255) NOT NULL,
    type enum('fund') NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction (
    id UUID NOT NULL PRIMARY KEY,
    amount integer NOT NULL,
    timestamp TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS isaFundTransaction (
    id UUID NOT NULL PRIMARY KEY,
    isaId UUID REFERENCES isa(id),
    fundId UUID REFERENCES fund(id),
    transactionId UUID REFERENCES transaction(id)
);

