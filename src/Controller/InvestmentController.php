<?php

namespace Tom\CushonRetailIsa\Controller;

use Exception;
use Tom\CushonRetailIsa\Model\IsaRepository;
use NumberFormatter;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tom\CushonRetailIsa\model\IsaFundTransactionRepository;

class InvestmentController
{
    const ISA_LIMIT = 2000000;

    public function __construct(
        private IsaRepository                $isaRepository,
        private IsaFundTransactionRepository $isaFundtransaction,
        private NumberFormatter              $numberFormatter
    ) {}

    /**
     * #[Route('/invest/isa/fund', name: 'add_isa_fund_investment', methods: ['POST])]
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function addIsaFundInvestment(Request $request): Response
    {
        $customerId = $request->getPayload()->get('customerId');
        $fundId     = $request->getPayload()->get('fundId');
        $amount     = (int) $request->getPayload()->get('amount');
        $isa        = $this->isaRepository->getIsa($customerId, IsaRepository::TYPE_FUND)
            ?? $this->isaRepository->createIsa($customerId, IsaRepository::TYPE_FUND);

        if ($amount <= 0) {
            throw new BadRequestException('You must add a positive amount to invest');
        }

        $consideration = (int) $isa['yearToDate'] + $amount;
        if ($consideration > self::ISA_LIMIT) {
            $availableToInvest = max([$consideration - self::ISA_LIMIT, 0]);
            $formattedAmount = $this->numberFormatter->formatCurrency($availableToInvest / 100, 'GBP');
            $message = sprintf('You have £%d left of your allowance to invest', $formattedAmount);

            throw new BadRequestException($message);
        }

        $transaction = $this->isaFundtransaction->createIsaFundTransaction($isa['id'], $fundId, $amount);
        return new JsonResponse($transaction);
    }
}