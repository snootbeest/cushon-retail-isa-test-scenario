<?php

namespace Tom\CushonRetailIsa\Controller;

use Tom\CushonRetailIsa\Model\FundRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

readonly class FundController
{
    public function __construct(
        private FundRepository $fundRepository,
    ) {}

    /**
     * List all available funds
     * #[Route('/fund', name: 'fund_list', methods: ['GET])]
     * @return Response
     */
    public function getFunds(): Response
    {
        $funds = $this->fundRepository->getAll();

        return new JsonResponse($funds);
    }
}
