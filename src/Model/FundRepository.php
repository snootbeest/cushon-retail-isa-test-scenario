<?php

namespace Tom\CushonRetailIsa\Model;

class FundRepository
{
    /**
     * Fetch all the funds
     * @return array
     */
    public function getAll(): array
    {
        return [
            [
                'id'   => '79544961-d741-4c8b-ab44-dd10bf4f5daf',
                'name' => 'Cushon Equities Fund'
            ]
        ];
    }
}