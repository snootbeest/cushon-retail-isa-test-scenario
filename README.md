# Cushon Retail ISA Test Scenario

## Database
* The database schema can be found in [./database/schema.sql](./database/schema.sql)
* The database can be created (I used a local docker container and connected to it using itellij idea)
* Character sets and collation have been left as default
* It is assumed that transactions are started, committed and rolled back elsewhere in the application.

## Code
* The code can be found in the [./src](./src) directory
* What is provided is non-functional pseudocode
* Composer install will run and should satisfy your IDE
* In the interests of brevity the following decisions were made:
  * Controllers contain all application + business logic
  * Doctrine Entity declarations were omitted
  * Repositories are vestigial, do not connect to a database and return arrays
